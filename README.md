## 项目说明
- maku-cloud 是采用Spring Cloud Alibaba、SpringSecurity、Spring Cloud Gateway、SpringBoot、Nacos、Redis、Mybatis-Plus等框架，开发的一套SpringCloud快速开发系统，使用门槛极低，且采用MIT开源协议，完全免费开源，可免费用于**商业项目**等场景。
- 开发文档：[https://maku.net/docs/maku-cloud](https://maku.net/docs/maku-cloud)
- 演示环境：https://demo.maku.net/maku-cloud


## 前端工程
- Github仓库：https://github.com/makunet/maku-admin
- Gitee仓库：https://gitee.com/makunet/maku-admin


## 代码生成器
- Github仓库：https://github.com/makunet/maku-generator
- Gitee仓库：https://gitee.com/makunet/maku-generator


## 交流和反馈
- 官方社区：https://maku.net
- Github仓库：https://github.com/makunet/maku-cloud
- Gitee仓库：https://gitee.com/makunet/maku-cloud
- 技术解答、交流、反馈、建议等，请移步到官方社区，我们会及时回复，也方便今后的小伙伴寻找答案，感谢理解！


## 微信交流群
为了更好的交流，我们新提供了微信交流群，需扫描下面的二维码，关注公众号，回复【加群】，根据提示信息，作者会拉你进群的，感谢配合！

![](https://maku.net/app/img/qrcode.jpg)


## 效果图
![输入图片说明](images/1.jpg)

![输入图片说明](images/2.jpg)

![输入图片说明](images/3.jpg)

![输入图片说明](images/4.jpg)

![输入图片说明](images/5.jpg)

## 支持
如果觉得框架还不错，或者已经在使用了，希望你可以去 [Github](https://github.com/makunet/maku-cloud) 或 [Gitee](https://gitee.com/makunet/maku-cloud) 帮作者点个 ⭐ Star，这将是对作者极大的鼓励与支持。
